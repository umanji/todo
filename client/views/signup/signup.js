
Template.signup.onCreated(function() {
});

Template.signup.helpers({
  inviteCode() {
    return '초대코드 : ' + FlowRouter.getParam('inviteCode');
  }
});

Template.signup.events({
  'submit form': (e) => {
    e.preventDefault();

    var user = {
      email:    $('#email').val(),
      password: $('#password').val(),
    }

    Accounts.createUser(user, (err) => {
      if(err) {
        sAlert.error(err.reason);
      } else {
        console.log('user is created');
      }
    });
  },

  'click .index-header': (e) => {
    FlowRouter.go('/');
  }
});
