
Template.post.onCreated(function() {
  var self = this;
  self.ready = new ReactiveVar();
  self.autorun(function() {
    var postId = FlowRouter.getParam('postId');
    var handle = PostSubs.subscribe('post', postId);
    self.ready.set(handle.ready());
  });
});

Template.post.helpers({
  post: function() {
    var postId = FlowRouter.getParam('postId');
    var post = Posts.findOne({_id: postId}) || {};
    return post;
  },

  postReady: function() {
    return Template.instance().ready.get();
  }
});
