
Template.index.onCreated(function() {
  Meteor.subscribe('system');
});

Template.index.onRendered(function() {
  Meteor.setInterval(function () {
    System.update({_id: 'system'}, {$inc:{userCount:1}})
  }, Math.floor((Math.random()*3000)) );

});

Template.index.helpers({
  userCount() {
    return System.findOne()? System.findOne().userCount.format(): 0;
  }
});

Template.index.events({
  'click #signup-btn': (e) => {
    e.preventDefault();

    var code = $('#inviteCode').val();
    if(code === '홍익인간') {
      FlowRouter.go('/in/' + code);
    } else {
      $('#exModal').modal('show');
    }
  }
})
