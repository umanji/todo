/********************************************
* route
*********************************************/

FlowRouter.route('/', {
  action: function() {
    BlazeLayout.render("fullLayout", {content: "index"});
  }
});

FlowRouter.route('/login', {
  action: function() {
    BlazeLayout.render("fullLayout", {content: "login"});
  }
});

FlowRouter.route('/signup', {
  action: function() {
    BlazeLayout.render("fullLayout", {content: "signup"});
  }
});

FlowRouter.route('/in/:inviteCode', {
  action: function() {
    BlazeLayout.render("fullLayout", {content: "signup"});
  }
});

FlowRouter.route('/home', {
  action: function() {
    BlazeLayout.render("mainLayout", {content: "home"});
  }
});

FlowRouter.route('/post/:postId', {
  action: function(params) {
    BlazeLayout.render("mainLayout", {
      content: "post",
      params: params
    });
  }
});
